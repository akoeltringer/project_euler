# -*- coding: utf-8 -*-

import pytest
from utils import is_prime


class TestIsPrime:
    @staticmethod
    @pytest.mark.parametrize(
        "value", [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61],
    )
    def test_is_prime(value):
        assert is_prime(value)
        assert is_prime(-value)

    @staticmethod
    @pytest.mark.parametrize(
        "value",
        [1, 4, 6, 8, 9, 10, 12, 14, 15, 16, 18, 20, 21, 22, 24, 25, 26, 27, 28, 30, 32],
    )
    def test_is_not_prime(value):
        assert not is_prime(value)
        assert not is_prime(-value)
