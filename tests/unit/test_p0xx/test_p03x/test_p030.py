# -*- coding: utf-8 -*-

import pytest
from p0xx.p03x import p030


@pytest.mark.parametrize("num", [1634, 8208, 9474])
def test_sum_digits_pow_is_num(num):
    assert p030.sum_digits_pow_is_num(num, 4)


@pytest.mark.parametrize(
    "num", [i for i in range(10, 10000) if i not in [1634, 8208, 9474]]
)
def test_sum_digits_pow_is_not_num(num):
    assert not p030.sum_digits_pow_is_num(num, 4)


def test_brute_force():
    assert p030.brute_force(4) == 19316
