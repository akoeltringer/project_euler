# -*- coding: utf-8 -*-

import pytest
from p0xx.p02x import p027


@pytest.mark.parametrize("a,b,expected", [(1, 41, 40), (-79, 1601, 80)])
def test_alphabetical_value_upper(a: int, b: int, expected: int):
    assert p027.find_max_n((a, b)) == (a, b, expected)
