# -*- coding: utf-8 -*-

from p0xx.p02x import p020


def test_sum_of_digits_of_factorial():
    assert p020.sum_of_digits_of_factorial(10) == 27
