# -*- coding: utf-8 -*-

from p0xx.p02x import p029


def test_brute_force():
    assert p029.brute_force(5) == 15
