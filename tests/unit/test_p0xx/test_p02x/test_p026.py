# -*- coding: utf-8 -*-

import pytest
from p0xx.p02x import p026


@pytest.mark.parametrize(
    "actual,expected", [(7, 6), (3, 1), (4, 0), (6, 1), (9, 1),],
)
def test_divide_one_by(actual, expected):
    assert p026.divide_one_by(actual) == expected
