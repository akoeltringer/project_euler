# -*- coding: utf-8 -*-

import pytest
from p0xx.p02x import p022


@pytest.mark.parametrize("actual,expected", [("ABC", 6), ("COLIN", 53)])
def test_alphabetical_value_upper(actual: str, expected: int):
    assert p022.alphabetical_value(actual) == expected


@pytest.mark.parametrize("actual", ["abc", "colin", "COL1", "Colin", "COLIN+"])
def test_alphabetical_value_non_upper(actual):
    with pytest.raises(ValueError):
        p022.alphabetical_value(actual)


def test_brute_force():
    names = ["ALICE", "BOB", "CARL"]
    assert p022.brute_force(names) == 170
