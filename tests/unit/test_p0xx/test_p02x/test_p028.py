# -*- coding: utf-8 -*-

from p0xx.p02x import p028


def test_walk_grid():
    Vec2D = p028.Vec2D
    assert p028.walk_grid(3) == [
        (Vec2D(0, 0), 1),
        (Vec2D(1, 0), 2),
        (Vec2D(1, -1), 3),
        (Vec2D(0, -1), 4),
        (Vec2D(-1, -1), 5),
        (Vec2D(-1, 0), 6),
        (Vec2D(-1, 1), 7),
        (Vec2D(0, 1), 8),
        (Vec2D(1, 1), 9),
    ]


def test_sum_diagonals():
    assert p028.sum_diagonals(p028.walk_grid(5)) == 101


def test_sum_by_nth_element():
    assert p028.sum_by_nth_element(5) == 101
