# -*- coding: utf-8 -*-

import itertools
from p0xx.p02x import p025


def test_fibonacci_sequence():
    assert list(itertools.islice(p025.fibonacci_sequence(), 12)) == (
        [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144]
    )


def test_brute_force():
    assert p025.brute_force(3) == 12
