# -*- coding: utf-8 -*-

import datetime
import pytest
from p0xx.p01x import p019


@pytest.mark.parametrize(
    "actual,expected",
    [
        (1600, True),
        (1700, False),
        (1800, False),
        (1900, False),
        (1996, True),
        (1997, False),
        (1998, False),
        (1999, False),
        (2000, True),
        (2001, False),
        (2002, False),
        (2003, False),
        (2004, True),
        (2100, False),
        (2200, False),
        (2300, False),
        (2400, True),
    ],
)
def test_is_leap_year(actual, expected):
    assert p019.is_leap_year(actual) == expected


@pytest.mark.parametrize(
    "actual,expected",
    zip(
        p019.generate_dates(),
        [
            datetime.datetime(1900, 1, 1) + datetime.timedelta(days=days)
            for days in range(3650)
        ],
    ),
)
def test_generate_dates(actual, expected):
    actual_dt = datetime.datetime(actual[0], actual[1], actual[2])
    assert actual_dt == expected
    assert actual[3] == expected.weekday()
