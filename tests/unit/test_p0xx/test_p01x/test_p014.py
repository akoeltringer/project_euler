# -*- coding: utf-8 -*-

from p0xx.p01x import p014


def test_collatz():
    assert list(p014.collatz(13)) == [13, 40, 20, 10, 5, 16, 8, 4, 2, 1]


def test_generator_len():
    assert p014.generator_len(p014.collatz, 13) == 10
