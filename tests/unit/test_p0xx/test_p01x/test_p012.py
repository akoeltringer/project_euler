# -*- coding: utf-8 -*-

import pytest
from p0xx.p01x import p012


divisor_examples = [
    (1, {1}),
    (3, {1, 3}),
    (6, {1, 2, 3, 6}),
    (10, {1, 2, 5, 10}),
    (15, {1, 3, 5, 15}),
    (21, {1, 3, 7, 21}),
    (28, {1, 2, 4, 7, 14, 28}),
]


@pytest.mark.parametrize(
    "actual,expected",
    [(1, 1), (2, 3), (3, 6), (4, 10), (5, 15), (6, 21), (7, 28), (10, 55)],
)
def test_sum_of_first(actual, expected):
    assert p012.sum_of_first(actual) == expected


@pytest.mark.parametrize("actual,expected", divisor_examples)
def test_find_divisors(actual, expected):
    assert p012.find_divisors(actual) == expected


@pytest.mark.parametrize("actual,expected", divisor_examples)
def test_find_divisors_v2(actual, expected):
    assert p012.find_divisors_v2(actual) == expected


def test_brute_force():
    assert p012.brute_force(5) == 28
