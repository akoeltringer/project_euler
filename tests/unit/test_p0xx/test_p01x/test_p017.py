# -*- coding: utf-8 -*-

import pytest
from p0xx.p01x import p017


CASES = [
    *[(k, v) for (k, v) in p017.NUM_DICT.items() if k < 100],
    (31, "thirtyone"),
    (42, "fortytwo"),
    (53, "fiftythree"),
    (64, "sixtyfour"),
    (75, "seventyfive"),
    (86, "eightysix"),
    (97, "ninetyseven"),
    (100, "onehundred"),
    (101, "onehundredandone"),
    (110, "onehundredandten"),
    (111, "onehundredandeleven"),
    (220, "twohundredandtwenty"),
    (335, "threehundredandthirtyfive"),
    (500, "fivehundred"),
    (1000, "onethousand"),
]


@pytest.mark.parametrize("actual,expected", CASES)
def test_num_to_text_base_cases(actual, expected):
    assert p017.num_to_text(actual) == expected


def test_char_count_via_num_to_text():
    assert p017.count_via_num_to_text(5) == 19
