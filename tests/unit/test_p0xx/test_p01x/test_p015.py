# -*- coding: utf-8 -*-

import pytest
from p0xx.p01x import p015


def test_brute_force_recursive():
    assert p015.brute_force_recursive(2, 2) == 6


@pytest.mark.parametrize("n,r,expected", [(4, 2, 6)])
def test_nCr(n, r, expected):
    assert p015.nCr(n, r) == expected


def test_analytic_solution():
    assert p015.analytic_solution(2) == 6
