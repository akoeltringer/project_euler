# -*- coding: utf-8 -*-

from p0xx.p01x import p016


def test_via_text():
    assert p016.via_text(15) == 26


def test_via_math():
    assert p016.via_math(15) == 26
