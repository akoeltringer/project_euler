# -*- coding: utf-8 -*-

import pytest
from p0xx.p01x import p018


@pytest.mark.parametrize(
    "actual,expected",
    [
        ("0\n1 2", [[0], [1, 2]]),
        ("\n0\n1 2\n", [[0], [1, 2]]),
        ("3\n4 5\n6 7 8", [[3], [4, 5], [6, 7, 8]]),
    ],
)
def test_parse_triangle(actual, expected):
    assert p018.parse_triangle(actual) == expected


@pytest.mark.parametrize(
    "actual,expected",
    [
        ([[0], [1, 2]], [[0, 1], [0, 2]]),
        ([[0], [1, 2], [3, 4, 5]], [[0, 1, 3], [0, 1, 4], [0, 2, 4], [0, 2, 5]]),
    ],
)
def test_find_all_paths_brute_force(actual, expected):
    assert p018.find_all_paths_brute_force(0, 0, [], actual) == expected


def test_max_path_brute_force():
    assert p018.max_path_brute_force([[3], [7, 4], [2, 4, 6], [8, 5, 9, 3]]) == 23
