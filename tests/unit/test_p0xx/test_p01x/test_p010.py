# -*- coding: utf-8 -*-

from p0xx.p01x import p010


def test_brute_force():
    assert p010.brute_force(10) == 17
