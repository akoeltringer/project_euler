# -*- coding: utf-8 -*-

import pytest
from p0xx.p00x import p006


@pytest.mark.parametrize("actual,expected", [(3, 14), (10, 385)])
def test_sum_of_squares(actual, expected):
    assert p006.sum_of_squares(actual) == expected


@pytest.mark.parametrize("actual,expected", [(3, 36), (10, 3025)])
def test_square_of_sum(actual, expected):
    assert p006.square_of_sum(actual) == expected


@pytest.mark.parametrize("actual,expected", [(3, 22), (10, 2640)])
def test_square_of_sum(actual, expected):
    assert p006.diff(actual) == expected
