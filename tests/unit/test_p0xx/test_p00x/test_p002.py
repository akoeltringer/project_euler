# -*- coding: utf-8 -*-

import pytest

from p0xx.p00x import p002


def test_fib_loop():
    assert p002.fib_loop(90) == 44


class TestFibRecursive:
    @staticmethod
    @pytest.mark.parametrize(
        "n,expected", [(3, 3), (4, 5), (5, 8), (6, 13), (7, 21), (10, 89)]
    )
    def test_fib_recursive(n, expected):
        assert p002.fib_recursive(n) == expected

    @staticmethod
    @pytest.mark.parametrize(
        "n,expected", [(3, 3), (4, 5), (5, 8), (6, 13), (7, 21), (10, 89)]
    )
    def test_fib_recursive_memoized(n, expected):
        assert p002.fib_recursive_memoized(n) == expected

    @staticmethod
    def test_sum_of_even_recursive():
        assert p002.sum_of_even_recursive(90) == 44
