# -*- coding: utf-8 -*-

from p0xx.p00x import p001


def test_check_all_numbers():
    assert p001.check_all_numbers(10) == 23


def test_use_only_multiples():
    assert p001.use_only_multiples(10) == 23


def test_analytic():
    assert p001.analytic(10) == 23
