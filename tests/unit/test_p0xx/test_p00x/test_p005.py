# -*- coding: utf-8 -*-

import pytest
from p0xx.p00x import p005


@pytest.mark.parametrize(
    "actual,expected", [(2520, True), (3628800, True), (10, False)]
)
def test_is_divisible_by_all(actual, expected):
    assert p005.is_divisible_by_all(actual, 10) == expected


def test_brute_force():
    assert p005.brute_force(10) == 2520


def test_analytic():
    assert p005.analytic(10) == 2520
