# -*- coding: utf-8 -*-

import pytest
from p0xx.p00x import p003


@pytest.mark.parametrize(
    "actual,expected",
    [
        (12, {2: 2, 3: 1}),
        (15, {3: 1, 5: 1}),
        (20, {2: 2, 5: 1}),
        (13195, {5: 1, 7: 1, 13: 1, 29: 1}),
    ],
)
def test_prime_factors_brute_force(actual, expected):
    assert p003.prime_factors_brute_force(actual) == expected
