# -*- coding: utf-8 -*-

import pytest
from p0xx.p00x import p004


@pytest.mark.parametrize("actual,expected", [(123, False), (121, True), (9009, True)])
def test_is_palindrome(actual, expected):
    assert p004.is_palindrome(actual) == expected


def test_search_forward():
    assert p004.search_forward(2) == 9009
