# -*- coding: utf-8 -*-

from p0xx.p00x import p008


def test_brute_force():
    assert p008.brute_force(4) == 5832
