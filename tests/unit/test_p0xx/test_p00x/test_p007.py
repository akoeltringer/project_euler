# -*- coding: utf-8 -*-

from p0xx.p00x import p007


def test_nth_prime_brute_force():
    assert p007.nth_prime_brute_force(6) == 13
