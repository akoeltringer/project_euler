# -*- coding: utf-8 -*-

import math
import time
from typing import Generator


def is_prime(n: int) -> bool:
    """check if a number is prime."""
    n = abs(n)
    if n == 1:
        return False

    upper = int(math.sqrt(n)) + 1 if n > 4 else n
    for i in range(2, upper):
        if n % i == 0:
            return False

    return True


def natural_numbers(start: int = 0) -> Generator[int, None, None]:
    """Generator to loop over the natural numbers"""
    i = start
    while True:
        yield i
        i += 1


def timeit(func, *args, **kwargs):
    """timing function calls"""
    t1 = time.time()
    res = func(*args, **kwargs)
    t2 = time.time()
    print(f"Execution of {func.__name__} took {(t2-t1)*1000}ms. Result: {res}.")
