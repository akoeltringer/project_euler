# -*- coding: utf-8 -*-

"""
2520 is the smallest number that can be divided by each of the numbers from 1 to 10
without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers
from 1 to 20?
"""
import math
from typing import Dict
from p0xx.p00x.p003 import prime_factors_brute_force
from utils import timeit


def is_divisible_by_all(n: int, upper: int) -> bool:
    for i in range(2, upper + 1):
        if n % i != 0:
            return False

    return True


def brute_force(upper: int) -> int:
    i = upper
    while True:
        if is_divisible_by_all(i, upper):
            return i
        i += 1


def update_prime_factors(all_factors: Dict[int, int], new_factors: Dict[int, int]):
    """Update the ``all_factors`` dict to have the common prime factors, i.e.
    keep the maximum count for each prime factor.
    """
    for key, value in new_factors.items():
        all_factors[key] = max(all_factors.get(key, 0), value)

    return all_factors


def analytic(upper: int) -> int:
    """multiplying the common prime factors of the numbers gives the solution"""
    all_factors = {}
    for i in range(2, upper + 1):
        factors_i = prime_factors_brute_force(i)
        all_factors = update_prime_factors(all_factors, factors_i)

    print(all_factors)
    return math.prod([k ** v for k, v in all_factors.items()])


def main():
    timeit(brute_force, 20)
    timeit(analytic, 20)


if __name__ == "__main__":
    main()
