# -*- coding: utf-8 -*-

"""
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""
from typing import Dict
from utils import timeit


def prime_factors_brute_force(n: int) -> Dict[int, int]:
    """Compute the prime factors via brute force. Returns a dictionary which has
    the factors as keys and their counts as values, i.e.
    {factor: value}
    """
    factors = dict()
    i = 2
    while True:
        if n == 1:
            break

        if n % i == 0:
            factors[i] = factors.get(i, 0) + 1
            n /= i
            continue

        i += 1

    return factors


def max_factor(n, func=prime_factors_brute_force):
    return max(x for x in func(n).keys())


def main():
    timeit(max_factor, 600851475143)


if __name__ == "__main__":
    main()
