# -*- coding: utf-8 -*-

"""
A palindromic number reads the same both ways. The largest palindrome made from the
product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""

from utils import timeit


def is_palindrome(n: int) -> bool:
    n_str = str(n)
    # return n_str == n_str[::-1]
    return n_str == "".join(reversed(n_str))


def search_forward(n: int) -> int:
    lower = 10 ** (n - 1)
    upper = 10 ** n
    result_list = []
    for first in range(lower, upper):
        for second in range(lower, upper):
            p = first * second
            if is_palindrome(p):
                result_list.append(p)

    return max(result_list)


def main() -> None:
    timeit(search_forward, 3)


if __name__ == "__main__":
    main()
