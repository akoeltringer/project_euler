# -*- coding: utf-8 -*-

"""
The sum of the squares of the first ten natural numbers is,
1^2 + 2^2 + ... + 10^2 = 385

The square of the sum of the first ten natural numbers is,
(1+2+...+10)^2 = 55^2 = 3025

Hence the difference between the sum of the squares of the first ten natural numbers
and the square of the sum is 3025 − 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural
numbers and the square of the sum.
"""

from utils import timeit


def sum_of_squares(n: int) -> int:
    return sum([i ** 2 for i in range(1, n + 1)])


def square_of_sum(n: int) -> int:
    return (n * (n + 1) // 2) ** 2


def diff(n: int) -> int:
    return square_of_sum(n) - sum_of_squares(n)


def main() -> None:
    timeit(diff, 100)


if __name__ == "__main__":
    main()
