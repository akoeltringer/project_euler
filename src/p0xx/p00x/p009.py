# -*- coding: utf-8 -*-

"""
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
a^2 + b^2 = c^2

For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
"""
import math
from utils import timeit


def brute_force():
    upper = 1000
    for a in range(1, upper):
        for b in range(a + 1, upper):
            for c in range(b + 1, upper):
                if a + b + c != 1000:
                    continue

                if a ** 2 + b ** 2 == c ** 2:
                    return a, b, c, a * b * c


def a_bit_smarter():
    upper = 1000
    for a in range(1, upper):
        for b in range(a + 1, upper):
            c = math.sqrt(a ** 2 + b ** 2)
            if c != int(c):
                continue
            c = int(c)
            if a + b + c == 1000:
                return a, b, c, a * b * c


def main() -> None:
    timeit(brute_force)
    timeit(a_bit_smarter)


if __name__ == "__main__":
    main()
