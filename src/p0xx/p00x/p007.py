# -*- coding: utf-8 -*-

"""
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that
the 6th prime is 13.

What is the 10 001st prime number?
"""
from utils import is_prime
from utils import timeit


def nth_prime_brute_force(n: int) -> int:
    candidate = 2
    i = 1
    while True:
        candidate += 1
        if is_prime(candidate):
            i += 1
        if i == n:
            return candidate


def main() -> None:
    timeit(nth_prime_brute_force, 10001)


if __name__ == "__main__":
    main()
