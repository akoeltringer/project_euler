# -*- coding: utf-8 -*-

"""
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get
3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
"""

from utils import timeit


def check_all_numbers(upper: int) -> int:
    """go trough all numbers, check if divisible by 3 or 5 and add if yes"""
    s = 0
    for i in range(1, upper):
        if (i % 3) == 0 or (i % 5) == 0:
            s += i
    return s


def use_only_multiples(upper):
    """get all multiples of 3 and 5. use set() to eliminate duplicates."""
    return sum(set(list(range(0, upper, 3)) + list(range(0, upper, 5))))


def analytic(upper):
    """
    * 3 + 6 + 9 + ... + n = 3 * (1 + 2 + 3 + ... + n / 3) = 3 * n/3*(n/3+1) / 2
    * same for 5: 5 * n/5 * (n/5+1) / 2
    * same for 15 - subtract numbers divisible by both
    """
    n = upper - 1
    p3 = 3 * (n // 3) * (n // 3 + 1) / 2
    p5 = 5 * (n // 5) * (n // 5 + 1) / 2
    p15 = 15 * (n // 15) * (n // 15 + 1) / 2
    return p3 + p5 - p15


def main() -> None:
    timeit(check_all_numbers, 1000)
    timeit(use_only_multiples, 1000)
    timeit(analytic, 1000)


if __name__ == "__main__":
    main()
