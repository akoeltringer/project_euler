# -*- coding: utf-8 -*-

"""
Surprisingly there are only three numbers that can be written as the sum of fourth
powers of their digits:

    1634 = 1^4 + 6^4 + 3^4 + 4^4
    8208 = 8^4 + 2^4 + 0^4 + 8^4
    9474 = 9^4 + 4^4 + 7^4 + 4^4

As 1 = 1^4 is not a sum it is not included.

The sum of these numbers is 1634 + 8208 + 9474 = 19316.

Find the sum of all the numbers that can be written as the sum of fifth powers of
their digits.
"""

from utils import natural_numbers, timeit


def sum_digits_pow_is_num(num: int, pow: int):
    return sum([int(digit) ** pow for digit in list(str(num))]) == num


def brute_force(pow: int) -> int:

    # find break condition: up to which number needs to be searched?
    # --> when is the sum of digits to the power of ``pow`` smaller than the biggest
    #     number with the same number of digits
    n_digits = 0
    for n_digits in natural_numbers(2):
        if 10 ** n_digits - 1 > n_digits * 9 ** pow:
            break

    nums = []
    for candidate in range(10, 10 ** n_digits):
        if sum_digits_pow_is_num(candidate, pow):
            nums.append(candidate)

    return sum(nums)


def main() -> None:
    timeit(brute_force, 5)


if __name__ == "__main__":
    main()
