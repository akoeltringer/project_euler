# -*- coding: utf-8 -*-

"""
Let d(n) be defined as the sum of proper divisors of n (numbers less than n which
divide evenly into n). If d(a) = b and d(b) = a, where a ≠ b, then a and b are an
amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110;
therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142;
so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.
"""
from typing import Set
from p0xx.p01x.p012 import find_divisors_v2
from utils import timeit


def find_proper_divisors(n: int) -> Set[int]:
    divisors = find_divisors_v2(n)
    divisors.remove(n)
    return divisors


def find_amicable_numbers(upper: int) -> Set[int]:
    amicable_numbers: Set[int] = set()
    for a in range(2, upper):
        if a in amicable_numbers:
            continue

        b = sum(find_proper_divisors(a))
        if a != b and sum(find_proper_divisors(b)) == a:
            amicable_numbers.add(a)
            amicable_numbers.add(b)

    return amicable_numbers


def main() -> None:
    timeit(lambda n: sum(find_amicable_numbers(n)), 10000)


if __name__ == "__main__":
    main()
