#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Euler discovered the remarkable quadratic formula:

n^2 + n + 41

It turns out that the formula will produce 40 primes for the consecutive integer values
0≤n≤39. However, when n=40, 40^2 + 40 + 41 = 40(40+1)+41 is divisible by 41, and
certainly when n=41,

    41^2 + 41 + 41

is clearly divisible by 41.

The incredible formula n^2 − 79n + 1601 was discovered, which produces 80 primes for
the consecutive values 0≤n≤79. The product of the coefficients, −79 and 1601, is
−126479.

Considering quadratics of the form:

    n^2 + an + b

where |a|<1000 and |b|≤1000
where |n| is the modulus/absolute value of n e.g. |11|=11 and |−4|=4

Find the product of the coefficients, a and b, for the quadratic expression that
produces the maximum number of primes for consecutive values of n, starting with n=0.
"""
import itertools
import multiprocessing
from typing import Tuple
from utils import is_prime, natural_numbers, timeit


def find_max_n(params: Tuple[int, int]) -> Tuple[int, int, int]:
    a, b = params
    for n in natural_numbers():
        if not is_prime(n ** 2 + a * n + b):
            return a, b, n


def brute_force(abs_a: int, abs_b: int) -> Tuple[int, int]:
    a_max, b_max, n_max = 0, 0, 0
    a_range = range(-abs_a, abs_a + 1)
    b_range = range(-abs_b, abs_b + 1)

    p = multiprocessing.Pool()
    results = p.imap(find_max_n, itertools.product(a_range, b_range), chunksize=1000)

    for a, b, n in results:
        if n > n_max:
            a_max, b_max, n_max = a, b, n

    return a_max * b_max, n_max


def main() -> None:
    timeit(brute_force, 999, 1000)


if __name__ == "__main__":
    main()
