# -*- coding: utf-8 -*-

"""
A perfect number is a number for which the sum of its proper divisors is exactly equal
to the number. For example, the sum of the proper divisors of 28 would be
1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.

A number n is called deficient if the sum of its proper divisors is less than n and it
is called abundant if this sum exceeds n.

As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that
can be written as the sum of two abundant numbers is 24. By mathematical analysis, it
can be shown that all integers greater than 28123 can be written as the sum of two
abundant numbers. However, this upper limit cannot be reduced any further by analysis
even though it is known that the greatest number that cannot be expressed as the sum of
two abundant numbers is less than this limit.

Find the sum of all the positive integers which cannot be written as the sum of two
abundant numbers.
"""
import itertools
from typing import List
from p0xx.p02x.p021 import find_proper_divisors
from utils import timeit


def get_abundand_numbers(upper: int) -> List[int]:
    return [num for num in range(2, upper) if sum(find_proper_divisors(num)) > num]


def analytic_solution() -> int:
    upper = 28123
    abundant_numbers = get_abundand_numbers(upper)
    # get all 2-elem combinations
    all_2_elem_sums = set(
        map(sum, itertools.combinations_with_replacement(abundant_numbers, 2))
    )
    all_pos_ints = set(range(1, upper + 1))
    nums_not_as_sum_of_two_abundant = all_pos_ints.difference(all_2_elem_sums)
    return sum(nums_not_as_sum_of_two_abundant)


def main() -> None:
    timeit(analytic_solution)


if __name__ == "__main__":
    main()
