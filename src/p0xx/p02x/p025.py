# -*- coding: utf-8 -*-

"""
The Fibonacci sequence is defined by the recurrence relation:

    Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.

Hence the first 12 terms will be:
    F1 = 1
    F2 = 1
    F3 = 2
    F4 = 3
    F5 = 5
    F6 = 8
    F7 = 13
    F8 = 21
    F9 = 34
    F10 = 55
    F11 = 89
    F12 = 144

The 12th term, F12, is the first term to contain three digits.
What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
"""
from typing import Generator
from utils import timeit


def fibonacci_sequence() -> Generator[int, None, None]:
    n1 = 1
    n2 = 0
    yield n1  # the first "1"
    while True:
        yield n1 + n2
        n1, n2 = n1 + n2, n1


def brute_force(n_digits: int) -> int:
    divisor = 10 ** (n_digits - 1)
    for i, fib in enumerate(fibonacci_sequence(), start=1):
        if fib / divisor > 1:
            return i


def main() -> None:
    timeit(brute_force, 1000)


if __name__ == "__main__":
    main()
