# -*- coding: utf-8 -*-

"""
Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5
spiral is formed as follows:

21 22 23 24 25
20  7  8  9 10
19  6  1  2 11
18  5  4  3 12
17 16 15 14 13

It can be verified that the sum of the numbers on the diagonals is 101.
What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the
same way?
"""
from typing import List, Tuple
from utils import natural_numbers, timeit


class Vec2D:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, other: "Vec2D") -> "Vec2D":
        return Vec2D(self.x + other.x, self.y + other.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __repr__(self):
        return f"Vec2D({self.x}, {self.y})"


direction_right = Vec2D(1, 0)
direction_left = Vec2D(-1, 0)
direction_up = Vec2D(0, 1)
direction_down = Vec2D(0, -1)
directions = [direction_up, direction_right, direction_down, direction_left]


def walk_grid(size: int) -> List[Tuple[Vec2D, int]]:
    if size % 2 != 1:
        raise ValueError("size parameter must be odd!")

    break_pos = Vec2D((size - 1) // 2, (size - 1) // 2)
    grid = []
    # starting condition
    pos = Vec2D(0, 0)
    direction = Vec2D(0, 0)
    dir_i = 0

    for val in natural_numbers(1):
        pos += direction
        grid.append((pos, val))

        if (direction != direction_right and abs(pos.x) == abs(pos.y)) or (
            direction == direction_right and pos.x == pos.y + 1
        ):
            dir_i = (dir_i + 1) % 4
            direction = directions[dir_i]

        if pos == break_pos:
            break

    return grid


def sum_diagonals(grid: List[Tuple[Vec2D, int]]) -> int:
    total = 0
    for pos, val in grid:
        if abs(pos.x) == abs(pos.y):
            total += val
    return total


def sum_by_nth_element(size: int) -> int:
    """
    compute by summing nth elements:
    1
    + 3 + 5 + 7 + 9      (step = 2)
    + 13 + 17 + 21 + 25  (step = 4)
    + ...                (step = 6)
    """
    num = 1
    step = 2
    total = num

    while num < size ** 2:
        for _ in range(4):
            num += step
            total += num

        step += 2

    return total


def main() -> None:
    timeit(lambda: sum_diagonals(walk_grid(1001)))
    timeit(sum_by_nth_element, 1001)


if __name__ == "__main__":
    main()
