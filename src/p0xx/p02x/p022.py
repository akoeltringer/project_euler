# -*- coding: utf-8 -*-

"""
Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing
over five-thousand first names, begin by sorting it into alphabetical order. Then
working out the alphabetical value for each name, multiply this value by its
alphabetical position in the list to obtain a name score.

For example, when the list is sorted into alphabetical order, COLIN, which is worth
3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a
score of 938 × 53 = 49714.

What is the total of all the name scores in the file?
"""
import csv
import pathlib
import string
from typing import List
from utils import timeit


def read_names() -> List[str]:
    names = []
    here = pathlib.Path(__file__).parent
    with (here / "p022_names.txt").open() as csvfile:
        reader = csv.reader(csvfile, delimiter=",", quotechar='"')
        for row in reader:
            names += row

    return names


def alphabetical_value(word: str) -> int:
    if set(word).difference(set(string.ascii_uppercase)) != set():
        raise ValueError("non-ascii-uppercase letters detected")

    value = 0
    for letter in word:
        value += ord(letter) - 64
    return value


def brute_force(names):
    return sum(
        [alphabetical_value(name) * (i + 1) for i, name in enumerate(sorted(names))]
    )


def main() -> None:
    timeit(brute_force, read_names())


if __name__ == "__main__":
    main()
