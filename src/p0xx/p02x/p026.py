# -*- coding: utf-8 -*-

"""
A unit fraction contains 1 in the numerator. The decimal representation of the unit
fractions with denominators 2 to 10 are given:

    1/2	= 	0.5
    1/3	= 	0.(3)
    1/4	= 	0.25
    1/5	= 	0.2
    1/6	= 	0.1(6)
    1/7	= 	0.(142857)
    1/8	= 	0.125
    1/9	= 	0.(1)
    1/10	= 	0.1

Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that
1/7 has a 6-digit recurring cycle.
Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its
decimal fraction part.
"""
from typing import Tuple
from utils import is_prime, timeit


def analytic_solution() -> None:
    # for "large primes" it holds that the periodicity of 1 / prime_number
    # is prime_number - 1
    # so: find the largest "large prime" number below 1000
    # https://en.wikipedia.org/wiki/Full_reptend_prime
    # https://oeis.org/A002371
    # https://en.wikipedia.org/wiki/Repeating_decimal#Other_properties_of_repetend_lengths

    # TODO: needs fixing
    for i in range(1000, 1, -1):
        if is_prime(i):
            return i

    raise ValueError("did not find any primes")


def divide_one_by(d: int) -> int:
    periodicity = 0
    remainder = 1
    fracs = []
    remainder_list = []
    while remainder > 0:
        frac, remainder = remainder // d, (remainder % d) * 10
        fracs.append(frac)
        if remainder in remainder_list:
            periodicity = len(remainder_list) - remainder_list.index(remainder)
            break

        remainder_list.append(remainder)

    return periodicity


def find_largest_periodicity() -> Tuple[int, int]:
    max_p, max_d = 0, 0

    for d in range(2, 1000):
        p = divide_one_by(d)
        if p > max_p:
            max_p, max_d = p, d

    return max_p, max_d


def main() -> None:
    # timeit(analytic_solution)
    timeit(find_largest_periodicity)


if __name__ == "__main__":
    main()
