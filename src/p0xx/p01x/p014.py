# -*- coding: utf-8 -*-

"""
The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:
13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms.
Although it has not been proved yet (Collatz Problem), it is thought that all starting
numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
"""
from typing import Generator
from utils import timeit


def collatz(start: int) -> Generator[int, None, None]:
    i = start
    while True:
        yield i
        if i == 1:
            return
        if i % 2 == 0:
            i //= 2
        else:
            i = 3 * i + 1


def generator_len(gen_func, *args, **kwargs) -> int:
    i = -1
    for i, _ in enumerate(gen_func(*args, **kwargs)):
        pass
    return i + 1


def brute_force():
    max_len = 0
    starting_value_max_len = None
    for i in range(2, 1_000_000):
        current_len = generator_len(collatz, i)
        if current_len > max_len:
            max_len = current_len
            starting_value_max_len = i

        print(
            f"\ri: {i}, len: {current_len}, max_i: {starting_value_max_len}, max_len: {max_len}",
            end="",
        )

    print("")
    return starting_value_max_len


def main() -> None:
    timeit(brute_force)


if __name__ == "__main__":
    main()
