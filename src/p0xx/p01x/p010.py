# -*- coding: utf-8 -*-

"""
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
"""

from utils import is_prime, timeit


def brute_force(upper: int) -> int:
    total = 0
    for i in range(2, upper):
        if is_prime(i):
            total += i

    return total


def main() -> None:
    timeit(brute_force, 2_000_000)


if __name__ == "__main__":
    main()
