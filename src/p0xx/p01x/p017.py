# -*- coding: utf-8 -*-

"""
If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there
are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words,
how many letters would be used?

NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two)
contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of
"and" when writing out numbers is in compliance with British usage.
"""

from utils import timeit

NUM_DICT = {
    1: "one",
    2: "two",
    3: "three",
    4: "four",
    5: "five",
    6: "six",
    7: "seven",
    8: "eight",
    9: "nine",
    10: "ten",
    11: "eleven",
    12: "twelve",
    13: "thirteen",
    14: "fourteen",
    15: "fifteen",
    16: "sixteen",
    17: "seventeen",
    18: "eighteen",
    19: "nineteen",
    20: "twenty",
    30: "thirty",
    40: "forty",
    50: "fifty",
    60: "sixty",
    70: "seventy",
    80: "eighty",
    90: "ninety",
    100: "hundred",
    1000: "thousand",
}


def num_to_text(num: int) -> str:

    if num > 9999:
        raise ValueError(f"I am not programmed to work with such numbers: {num}")

    if num < 20:
        return NUM_DICT[num]

    if num < 100 and num % 10 == 0:
        return NUM_DICT[num]

    M = num // 1000
    H = (num % 1000) // 100
    T = ((num % 100) // 10) * 10
    O = num % 10

    num_str = ""
    if M != 0:
        num_str += NUM_DICT[M] + NUM_DICT[1000]

    if H != 0:
        num_str += NUM_DICT[H] + NUM_DICT[100]

    if H != 0 and (T != 0 or O != 0):
        num_str += "and"

    if T == 10:
        # ten, eleven, twelve, ...
        num_str += NUM_DICT[num % 100]
    else:
        if T != 0:
            num_str += NUM_DICT[T]

        if O != 0:
            num_str += NUM_DICT[O]

    return num_str


def count_via_num_to_text(upper: int) -> int:
    char_count = 0
    for i in range(1, upper + 1):
        char_count += len(num_to_text(i))
    return char_count


def main() -> None:
    timeit(count_via_num_to_text, 1000)


if __name__ == "__main__":
    main()
