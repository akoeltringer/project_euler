# -*- coding: utf-8 -*-

"""
Starting in the top left corner of a 2×2 grid, and only being able to move to the right
and down, there are exactly 6 routes to the bottom right corner.

How many such routes are there through a 20×20 grid?
"""
import math
from typing import List, Tuple
from utils import timeit


def brute_force_recursive(grid_x, grid_y):
    count = {"count": 0}  # need to use an object b/c of the scoping of variables

    def recurse(x: int, y: int, path: List[Tuple[int, int]]) -> None:
        if x < grid_x:
            recurse(x + 1, y, [*path, (x + 1, y)])

        if y < grid_y:
            recurse(x, y + 1, [*path, (x, y + 1)])

        if (x, y) == (grid_x, grid_y):
            count["count"] += 1

    recurse(0, 0, [])
    return count["count"]


def nCr(n, r):
    return math.factorial(n) // (math.factorial(r) * math.factorial(n - r))


def analytic_solution(grid_width: int) -> int:
    """This problem is basically the same as the Urn problem [1], namely, how to
    draw balls ("right", "down") out of an urn (the grid). The urn has 2 * grid_width
    balls, and the balls are drawn without replacement, and without considering the
    order.
    [1] https://en.wikipedia.org/wiki/Urn_problem
    """
    return nCr(2 * grid_width, grid_width)


def main() -> None:
    # takes too long!
    # timeit(brute_force_recursive, 20, 20)
    timeit(analytic_solution, 20)


if __name__ == "__main__":
    main()
