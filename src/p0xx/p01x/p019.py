# -*- coding: utf-8 -*-

"""
You are given the following information, but you may prefer to do some research for
yourself.

* 1 Jan 1900 was a Monday.
* Thirty days has September,
  April, June and November.
  All the rest have thirty-one,
  Saving February alone,
  Which has twenty-eight, rain or shine.
  And on leap years, twenty-nine.
* A leap year occurs on any year evenly divisible by 4, but not on a century unless
  it is divisible by 400.

How many Sundays fell on the first of the month during the twentieth century
(1 Jan 1901 to 31 Dec 2000)?
"""
import datetime
from typing import Generator, Tuple
from utils import timeit


def is_leap_year(year: int) -> bool:
    return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)


def generate_dates() -> Generator[Tuple[int, int, int, int], None, None]:

    year = 1900
    month = 1
    day = 1
    weekday = 0  # 0 = Monday, 6 = Sunday

    while True:
        yield year, month, day, weekday

        weekday = (weekday + 1) % 7

        if month in [4, 6, 9, 11] and day == 30:
            month += 1
            day = 1

        elif month in [1, 3, 5, 7, 8, 10] and day == 31:
            month += 1
            day = 1

        elif month == 12 and day == 31:
            year += 1
            month = 1
            day = 1

        elif month == 2 and is_leap_year(year) and day == 29:
            month += 1
            day = 1

        elif month == 2 and not is_leap_year(year) and day == 28:
            month += 1
            day = 1

        else:
            day += 1


def count_sundays():
    count = 0
    for year, month, day, weekday in generate_dates():
        if year >= 2001:
            break

        if year < 1901:
            continue

        if day == 1 and weekday == 6:
            count += 1

    return count


def count_using_datetime():
    count = 0
    for year in range(1901, 2001):
        for month in range(1, 13):
            if datetime.datetime(year, month, 1).weekday() == 6:
                count += 1

    return count


def main() -> None:
    timeit(count_sundays)
    timeit(count_using_datetime)


if __name__ == "__main__":
    main()
