# -*- coding: utf-8 -*-

"""
2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

What is the sum of the digits of the number 2^1000?
"""

from utils import timeit


def via_text(n: int) -> int:
    return sum([int(x) for x in list(str(2 ** n))])


def via_math(n: int) -> int:
    remainder = 2 ** n
    total = 0

    while remainder > 0:
        total += remainder % 10
        remainder //= 10

    return total


def main() -> None:
    timeit(via_text, 1000)
    timeit(via_math, 1000)


if __name__ == "__main__":
    main()
